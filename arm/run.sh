#perf libomp
LD_LIBRARY_PATH=$HOME/arm/install-libkomp/lib:$LD_LIBRARY_PATH KMP_ENABLE_TASK_THROTTLING=0 OMP_NUM_THREADS="28" OMP_PLACES="{0}:28:1" $HOME/kastors/build-gcc-arm/jacobi/jacobi_block_taskdep -n 16768 -b 131 -i 5 >data_libomp_bs_131_arm.log
LD_LIBRARY_PATH=$HOME/arm/install-libkomp/lib:$LD_LIBRARY_PATH KMP_ENABLE_TASK_THROTTLING=0 OMP_NUM_THREADS="28" OMP_PLACES="{0}:28:1" $HOME/kastors/build-gcc-arm/jacobi/jacobi_block_taskdep -n 16384 -b 128 -i 5 >data_libomp_bs_128_arm.log

#perf hashmaps
LD_LIBRARY_PATH=$HOME/arm/install-libkomp-hashmaps/lib:$LD_LIBRARY_PATH KMP_ENABLE_TASK_THROTTLING=0 OMP_NUM_THREADS="28" OMP_PLACES="{0}:28:1" $HOME/kastors/build-gcc-arm/jacobi/jacobi_block_taskdep -n 16768 -b 131 -i 5 >data_hashmaps_libomp_bs_131_arm.log
LD_LIBRARY_PATH=$HOME/arm/install-libkomp-hashmaps/lib:$LD_LIBRARY_PATH KMP_ENABLE_TASK_THROTTLING=0 OMP_NUM_THREADS="28" OMP_PLACES="{0}:28:1" $HOME/kastors/build-gcc-arm/jacobi/jacobi_block_taskdep -n 16384 -b 128 -i 5 >data_hashmaps_libomp_bs_128_arm.log

#log size prime

LD_LIBRARY_PATH=$HOME/arm/install-libkomp-hashmaps-logprimesize/lib:$LD_LIBRARY_PATH KMP_ENABLE_TASK_THROTTLING=0 OMP_NUM_THREADS="28" OMP_PLACES="{0}:28:1" $HOME/kastors/build-gcc-arm/jacobi/jacobi_block_taskdep -n 16768 -b 131 -i 5 2>data_hash_libomp_bs_131_prime_sizes_arm.csv
LD_LIBRARY_PATH=$HOME/arm/install-libkomp-hashmaps-logprimesize/lib:$LD_LIBRARY_PATH KMP_ENABLE_TASK_THROTTLING=0 OMP_NUM_THREADS="28" OMP_PLACES="{0}:28:1" $HOME/kastors/build-gcc-arm/jacobi/jacobi_block_taskdep -n 16384 -b 128 -i 5 2>data_hash_libomp_bs_128_prime_sizes_arm.csv

#log size x2
LD_LIBRARY_PATH=$HOME/arm/install-libkomp-hashmaps-logevensize/lib:$LD_LIBRARY_PATH KMP_ENABLE_TASK_THROTTLING=0 OMP_NUM_THREADS="28" OMP_PLACES="{0}:28:1" $HOME/kastors/build-gcc-arm/jacobi/jacobi_block_taskdep -n 16768 -b 131 -i 5 2>data_hash_libomp_bs_131_even_sizes_arm.csv
LD_LIBRARY_PATH=$HOME/arm/install-libkomp-hashmaps-logevensize/lib:$LD_LIBRARY_PATH KMP_ENABLE_TASK_THROTTLING=0 OMP_NUM_THREADS="28" OMP_PLACES="{0}:28:1" $HOME/kastors/build-gcc-arm/jacobi/jacobi_block_taskdep -n 16384 -b 128 -i 5 2>data_hash_libomp_bs_128_even_sizes_arm.csv
