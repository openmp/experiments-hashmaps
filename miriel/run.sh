#perf libomp
LD_LIBRARY_PATH=$HOME/install-libkomp/lib:$LD_LIBRARY_PATH KMP_ENABLE_TASK_THROTTLING=0 OMP_NUM_THREADS="24" OMP_PLACES="{0}:24:1" $HOME/kastors/build-gcc-miriel/jacobi/jacobi_block_taskdep -n 16768 -b 131 -i 5 >data_libomp_bs_131.log
LD_LIBRARY_PATH=$HOME/install-libkomp/lib:$LD_LIBRARY_PATH KMP_ENABLE_TASK_THROTTLING=0 OMP_NUM_THREADS="24" OMP_PLACES="{0}:24:1" $HOME/kastors/build-gcc-miriel/jacobi/jacobi_block_taskdep -n 16384 -b 128 -i 5 >data_libomp_bs_128.log

#perf hashmaps
LD_LIBRARY_PATH=$HOME/install-libkomp-hashmaps/lib:$LD_LIBRARY_PATH KMP_ENABLE_TASK_THROTTLING=0 OMP_NUM_THREADS="24" OMP_PLACES="{0}:24:1" $HOME/kastors/build-gcc-miriel/jacobi/jacobi_block_taskdep -n 16768 -b 131 -i 5 >data_hashmaps_libomp_bs_131.log
LD_LIBRARY_PATH=$HOME/install-libkomp-hashmaps/lib:$LD_LIBRARY_PATH KMP_ENABLE_TASK_THROTTLING=0 OMP_NUM_THREADS="24" OMP_PLACES="{0}:24:1" $HOME/kastors/build-gcc-miriel/jacobi/jacobi_block_taskdep -n 16384 -b 128 -i 5 >data_hashmaps_libomp_bs_128.log

#log size prime

LD_LIBRARY_PATH=$HOME/install-libkomp-hashmaps-logprimesize/lib:$LD_LIBRARY_PATH KMP_ENABLE_TASK_THROTTLING=0 OMP_NUM_THREADS="24" OMP_PLACES="{0}:24:1" $HOME/kastors/build-gcc-miriel/jacobi/jacobi_block_taskdep -n 16768 -b 131 -i 5 2>data_hash_libomp_bs_131_prime_sizes.csv
LD_LIBRARY_PATH=$HOME/install-libkomp-hashmaps-logprimesize/lib:$LD_LIBRARY_PATH KMP_ENABLE_TASK_THROTTLING=0 OMP_NUM_THREADS="24" OMP_PLACES="{0}:24:1" $HOME/kastors/build-gcc-miriel/jacobi/jacobi_block_taskdep -n 16384 -b 128 -i 5 2>data_hash_libomp_bs_128_prime_sizes.csv

#log size x2
LD_LIBRARY_PATH=$HOME/install-libkomp-hashmaps-logevensize/lib:$LD_LIBRARY_PATH KMP_ENABLE_TASK_THROTTLING=0 OMP_NUM_THREADS="24" OMP_PLACES="{0}:24:1" $HOME/kastors/build-gcc-miriel/jacobi/jacobi_block_taskdep -n 16768 -b 131 -i 5 2>data_hash_libomp_bs_131_even_sizes.csv
LD_LIBRARY_PATH=$HOME/install-libkomp-hashmaps-logevensize/lib:$LD_LIBRARY_PATH KMP_ENABLE_TASK_THROTTLING=0 OMP_NUM_THREADS="24" OMP_PLACES="{0}:24:1" $HOME/kastors/build-gcc-miriel/jacobi/jacobi_block_taskdep -n 16384 -b 128 -i 5 2>data_hash_libomp_bs_128_even_sizes.csv
